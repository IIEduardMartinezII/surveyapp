-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-11-2020 a las 01:53:37
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `labsdb2`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_desplegar_preguntas` ()  BEGIN
SELECT id_pregunta, pregunta from preguntas ORDER BY RAND() LIMIT 4;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_desplegar_preguntas_abcd` ()  BEGIN
SELECT id_pregunta, pregunta, opc_a, opc_b, opc_c, opc_d from preguntas_abcd ORDER BY RAND() LIMIT 3;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_desplegar_preguntas_multi` ()  BEGIN

SELECT id_pregunta, pregunta, opc_a, opc_b, opc_c, opc_d from preguntas_multi ORDER BY RAND() LIMIT 3;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_datos_usuario` (IN `id_reg` INT, IN `sexo` VARCHAR(255), IN `edad` VARCHAR(255), IN `salario` VARCHAR(255), IN `provincia` VARCHAR(255))  BEGIN
 SET @s=CONCAT ("INSERT into datos_usuarios (id_reg, sexo, edad, salario, provincia) values (", id_reg ,", '", sexo ,"', '", edad ,"', '", salario ,"', '", provincia , "');"); 
 
 PREPARE stmt from @s;
 EXECUTE stmt;
 DEALLOCATE PREPARE stmt;
 
 END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_respuestas_abcd` (IN `id_reg` INT, IN `id_resp` INT, IN `resp` VARCHAR(225))  BEGIN
 SET @s=CONCAT ("INSERT into respuestas_abcd (id_reg, id_preg, respuesta) values (", id_reg ,", ", id_resp ,", '", resp ,"');"); 
 
 PREPARE stmt from @s;
 EXECUTE stmt;
 DEALLOCATE PREPARE stmt;
 
 END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_respuestas_multi` (IN `id_reg` INT, IN `id_resp` INT, IN `resp_a` VARCHAR(255), IN `resp_b` VARCHAR(255), IN `resp_c` VARCHAR(255), IN `resp_d` VARCHAR(255))  BEGIN
 SET @s=CONCAT ("INSERT into respuestas_multi (id_reg, id_preg, respuesta_a, respuesta_b, respuesta_c, respuesta_d) values (", id_reg ,", ", id_resp ,", '", resp_a ,"', '", resp_b ,"', '", resp_c , "', '", resp_d ,"');"); 
 
 PREPARE stmt from @s;
 EXECUTE stmt;
 DEALLOCATE PREPARE stmt;
 
 END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insertar_respuestas_sino` (IN `id_reg` INT, IN `id_resp` INT, IN `resp` VARCHAR(255))  BEGIN
 SET @s=CONCAT ("INSERT into respuestas_sino (id_reg, id_preg, respuesta) values (", id_reg ,", ", id_resp ,", '", resp ,"');"); 
 
 PREPARE stmt from @s;
 EXECUTE stmt;
 DEALLOCATE PREPARE stmt;
 
 END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_datos` ()  SELECT sexo, edad, salario, provincia FROM datos_usuarios$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_datos_filtro` (IN `param_campo` VARCHAR(255), IN `param_valor` VARCHAR(255))  BEGIN
    	
        SET @s = CONCAT("SELECT id_reg, sexo, edad, salario, provincia FROM datos_usuarios WHERE ", param_campo ," LIKE CONCAT('%", param_valor ,"%')");
        PREPARE stmt FROM @s;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
        
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_total_encuesta` ()  SELECT COUNT(id_reg) from datos_usuarios$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_total_encuesta_sexo` ()  SELECT (SELECT COUNT(id_reg) AS Mujer from datos_usuarios WHERE sexo = "mujer") AS Mujer, (SELECT COUNT(id_reg) AS Hombre FROM datos_usuarios WHERE sexo = "hombre") AS Hombre$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_usuarios`
--

CREATE TABLE `datos_usuarios` (
  `id_reg` int(11) NOT NULL,
  `sexo` varchar(255) NOT NULL,
  `edad` varchar(255) NOT NULL,
  `salario` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `datos_usuarios`
--

INSERT INTO `datos_usuarios` (`id_reg`, `sexo`, `edad`, `salario`, `provincia`) VALUES
(282636521, 'Mujer', '16-25', '600-1000', 'Veraguas'),
(715266366, 'Hombre', '16-25', '600-1000', 'Cocle'),
(1004974716, 'Hombre', '16-25', '600-1000', 'Cocle'),
(1326808570, 'Mujer', '36-45', '1600-2000', 'Herrera'),
(1659637523, 'Mujer', '26-35', '>2100', 'Cocle'),
(1712350367, 'Hombre', '16-25', '600-1000', 'Cocle');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE `preguntas` (
  `id_pregunta` int(11) NOT NULL,
  `pregunta` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id_pregunta`, `pregunta`) VALUES
(1, 'La tortuga es el animal mas rápido?'),
(2, 'El aguila arpia es unos de los animales en peligro de extincion?'),
(3, 'El organismo de un puerco se asemeja al de una persona?'),
(4, 'El koala es un animal simbolo de japon?'),
(5, 'La rana entra como un animal de tipo reptil?'),
(6, 'El leon es un animal territorial?'),
(7, 'El mejor amigo del hombre es el perro?'),
(8, 'La girafa es una animal marino?'),
(9, 'Crees que el megalodon existe hoy en dia?'),
(10, 'El dragon de comodo es un animal carroñero?'),
(11, 'El cocodrilo es uno de los animales de agua dulce?'),
(12, 'Es el leopardo el animal mas rapido del mundo?'),
(13, 'El elefante es el animal mas grande del mundo?'),
(14, 'El murcielago es un animal que se guia por los sonidos?'),
(15, 'Es la ballena el animal marino mas grande del mar?'),
(16, 'Los animales pueden ser domesticados?'),
(17, 'Considera que los animales deberian ser cazados?'),
(18, 'Los animales en peligro de extincion tienen un cuidado especial?'),
(19, 'El Koala es un animal salvaje?'),
(20, 'Los buhos es un animal nocturno?');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas_abcd`
--

CREATE TABLE `preguntas_abcd` (
  `id_pregunta` int(11) NOT NULL,
  `pregunta` varchar(225) NOT NULL,
  `opc_a` varchar(225) NOT NULL,
  `opc_b` varchar(225) NOT NULL,
  `opc_c` varchar(225) NOT NULL,
  `opc_d` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preguntas_abcd`
--

INSERT INTO `preguntas_abcd` (`id_pregunta`, `pregunta`, `opc_a`, `opc_b`, `opc_c`, `opc_d`) VALUES
(1, '¿Cuál de los siguientes animales tiene 4 patas?', 'Perro', 'Paloma', 'Otra paloma', 'Ninguna de las anteriores'),
(2, '¿Cuántas patas tiene un perro?', '1', '2', '3', '4'),
(3, '¿Cual es el animal que puede sobrevivir en el espacio?', 'Mono', 'Gallo', 'Perro', 'Oso de agua'),
(4, '¿Cuantos corazones tiene un pulpo?', '1', '2', '3', '4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas_multi`
--

CREATE TABLE `preguntas_multi` (
  `id_pregunta` int(11) NOT NULL,
  `pregunta` varchar(225) NOT NULL,
  `opc_a` varchar(225) NOT NULL,
  `opc_b` varchar(225) NOT NULL,
  `opc_c` varchar(225) NOT NULL,
  `opc_d` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `preguntas_multi`
--

INSERT INTO `preguntas_multi` (`id_pregunta`, `pregunta`, `opc_a`, `opc_b`, `opc_c`, `opc_d`) VALUES
(1, 'Seleccione todos los anfibios', 'Sapo', 'Rana', 'Perro', 'Cucaracha'),
(2, 'Cuales de los siguientes son mamiferos?', 'Gato', 'Delfin', 'Iguana', 'Sapo'),
(3, 'Seleccione el gato', 'Perro', 'Gato', 'Otro Perro', 'Ninguna de las anteriores'),
(4, 'Si pudieras ser un animal, cual serias?', 'Cucaracha', 'Lagartija', 'Rana', 'Mosquito');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas_abcd`
--

CREATE TABLE `respuestas_abcd` (
  `id_reg` int(11) NOT NULL,
  `id_preg` int(11) NOT NULL,
  `respuesta` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `respuestas_abcd`
--

INSERT INTO `respuestas_abcd` (`id_reg`, `id_preg`, `respuesta`) VALUES
(1712350367, 1, 'C'),
(1712350367, 2, 'C'),
(1712350367, 4, 'C'),
(1004974716, 1, 'C'),
(1004974716, 2, 'C'),
(1004974716, 4, 'C'),
(715266366, 1, 'C'),
(715266366, 2, 'C'),
(715266366, 4, 'C'),
(1326808570, 1, 'A'),
(1326808570, 3, 'D'),
(1326808570, 4, 'C'),
(282636521, 1, 'C'),
(282636521, 2, 'C'),
(282636521, 3, 'C'),
(1659637523, 1, 'D'),
(1659637523, 2, 'D'),
(1659637523, 3, 'D');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas_multi`
--

CREATE TABLE `respuestas_multi` (
  `id_reg` int(11) NOT NULL,
  `id_preg` int(11) NOT NULL,
  `respuesta_a` varchar(225) NOT NULL,
  `respuesta_b` varchar(225) NOT NULL,
  `respuesta_c` varchar(225) NOT NULL,
  `respuesta_d` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `respuestas_multi`
--

INSERT INTO `respuestas_multi` (`id_reg`, `id_preg`, `respuesta_a`, `respuesta_b`, `respuesta_c`, `respuesta_d`) VALUES
(1712350367, 1, 'TRUE', 'TRUE', 'FALSE', 'FALSE'),
(1712350367, 3, 'TRUE', 'TRUE', 'TRUE', 'TRUE'),
(1712350367, 4, 'FALSE', 'FALSE', 'TRUE', 'TRUE'),
(1004974716, 1, 'TRUE', 'TRUE', 'FALSE', 'FALSE'),
(1004974716, 3, 'TRUE', 'TRUE', 'TRUE', 'TRUE'),
(1004974716, 4, 'FALSE', 'FALSE', 'TRUE', 'TRUE'),
(715266366, 1, 'TRUE', 'TRUE', 'FALSE', 'FALSE'),
(715266366, 3, 'TRUE', 'TRUE', 'TRUE', 'TRUE'),
(715266366, 4, 'FALSE', 'FALSE', 'TRUE', 'TRUE'),
(1326808570, 1, 'TRUE', 'TRUE', 'FALSE', 'FALSE'),
(1326808570, 2, 'FALSE', 'TRUE', 'FALSE', 'FALSE'),
(1326808570, 4, 'TRUE', 'FALSE', 'FALSE', 'FALSE'),
(282636521, 1, 'FALSE', 'TRUE', 'TRUE', 'TRUE'),
(282636521, 2, 'FALSE', 'FALSE', 'TRUE', 'TRUE'),
(282636521, 4, 'FALSE', 'FALSE', 'TRUE', 'TRUE'),
(1659637523, 2, 'FALSE', 'FALSE', 'TRUE', 'TRUE'),
(1659637523, 3, 'FALSE', 'FALSE', 'TRUE', 'TRUE'),
(1659637523, 4, 'FALSE', 'FALSE', 'TRUE', 'TRUE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas_sino`
--

CREATE TABLE `respuestas_sino` (
  `id_reg` int(11) NOT NULL,
  `id_preg` int(11) NOT NULL,
  `respuesta` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `respuestas_sino`
--

INSERT INTO `respuestas_sino` (`id_reg`, `id_preg`, `respuesta`) VALUES
(1712350367, 1, 'no'),
(1712350367, 8, 'no'),
(1712350367, 13, 'no'),
(1712350367, 15, 'no'),
(1004974716, 1, 'no'),
(1004974716, 8, 'no'),
(1004974716, 13, 'no'),
(1004974716, 15, 'no'),
(715266366, 1, 'no'),
(715266366, 8, 'no'),
(715266366, 13, 'no'),
(715266366, 15, 'no'),
(1326808570, 5, 'no'),
(1326808570, 9, 'si'),
(1326808570, 10, 'si'),
(1326808570, 12, 'si'),
(282636521, 1, 'si'),
(282636521, 4, 'si'),
(282636521, 10, 'no'),
(282636521, 11, 'no'),
(1659637523, 10, 'si'),
(1659637523, 13, 'si'),
(1659637523, 19, 'si'),
(1659637523, 20, 'si');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datos_usuarios`
--
ALTER TABLE `datos_usuarios`
  ADD PRIMARY KEY (`id_reg`);

--
-- Indices de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- Indices de la tabla `preguntas_abcd`
--
ALTER TABLE `preguntas_abcd`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- Indices de la tabla `preguntas_multi`
--
ALTER TABLE `preguntas_multi`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- Indices de la tabla `respuestas_abcd`
--
ALTER TABLE `respuestas_abcd`
  ADD KEY `id_reg` (`id_reg`),
  ADD KEY `id_preg` (`id_preg`);

--
-- Indices de la tabla `respuestas_multi`
--
ALTER TABLE `respuestas_multi`
  ADD KEY `id_reg` (`id_reg`),
  ADD KEY `id_preg` (`id_preg`);

--
-- Indices de la tabla `respuestas_sino`
--
ALTER TABLE `respuestas_sino`
  ADD KEY `id_reg` (`id_reg`),
  ADD KEY `id_preg` (`id_preg`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `datos_usuarios`
--
ALTER TABLE `datos_usuarios`
  MODIFY `id_reg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2118698763;

--
-- AUTO_INCREMENT de la tabla `preguntas`
--
ALTER TABLE `preguntas`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `preguntas_abcd`
--
ALTER TABLE `preguntas_abcd`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `preguntas_multi`
--
ALTER TABLE `preguntas_multi`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `respuestas_abcd`
--
ALTER TABLE `respuestas_abcd`
  ADD CONSTRAINT `respuestas_abcd_ibfk_1` FOREIGN KEY (`id_reg`) REFERENCES `datos_usuarios` (`id_reg`),
  ADD CONSTRAINT `respuestas_abcd_ibfk_2` FOREIGN KEY (`id_preg`) REFERENCES `preguntas_abcd` (`id_pregunta`);

--
-- Filtros para la tabla `respuestas_multi`
--
ALTER TABLE `respuestas_multi`
  ADD CONSTRAINT `respuestas_multi_ibfk_1` FOREIGN KEY (`id_reg`) REFERENCES `datos_usuarios` (`id_reg`),
  ADD CONSTRAINT `respuestas_multi_ibfk_2` FOREIGN KEY (`id_preg`) REFERENCES `preguntas_multi` (`id_pregunta`);

--
-- Filtros para la tabla `respuestas_sino`
--
ALTER TABLE `respuestas_sino`
  ADD CONSTRAINT `respuestas_sino_ibfk_1` FOREIGN KEY (`id_reg`) REFERENCES `datos_usuarios` (`id_reg`),
  ADD CONSTRAINT `respuestas_sino_ibfk_2` FOREIGN KEY (`id_preg`) REFERENCES `preguntas` (`id_pregunta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
