<?php
require_once('modelo.php');

class archivo extends modeloCredencialesDB{

	public function __construct(){
		parent::__construct();

	}

	//es lo mismo que listar preguntas
	public function listar_preguntas(){

		$instruccion = "CALL sp_desplegar_preguntas()";

		$consulta=$this->_db->query($instruccion);
		$resultado=$consulta->fetch_all(MYSQLI_ASSOC);

		if($resultado){
			return $resultado;
			$resultado->close();
			$this->_db->close();
		}
	}

	public function listar_preguntas_abcd(){

		$instruccion = "CALL sp_desplegar_preguntas_abcd()";

		$consulta=$this->_db->query($instruccion);
		$resultado=$consulta->fetch_all(MYSQLI_ASSOC);

		if($resultado){
			return $resultado;
			$resultado->close();
			$this->_db->close();
		}
	}

	public function listar_preguntas_multi(){

		$instruccion = "CALL sp_desplegar_preguntas_multi()";

		$consulta=$this->_db->query($instruccion);
		$resultado=$consulta->fetch_all(MYSQLI_ASSOC);

		if($resultado){
			return $resultado;
			$resultado->close();
			$this->_db->close();
		}
	}

	public function guardarDatos($id_reg, $sexo, $edad, $salario, $provincia){



		$instruccion = "CALL sp_insertar_datos_usuario(".$id_reg.",'".$sexo."','".$edad."','".$salario."','".$provincia."')";

		$actualiza=$this->_db->query($instruccion);

		if ($actualiza) {
			return $actualiza; 
			$actualiza->close(); 
			$this->_db->close();
		}

	}

	public function guardarRespuestas($id_reg, $id_preg, $respuesta){

		$instruccion = "CALL sp_insertar_respuestas_sino(".$id_reg.",".$id_preg.",'".$respuesta."')";

		$actualiza=$this->_db->query($instruccion);

		if ($actualiza) {
			return $actualiza; 
			$actualiza->close(); 
			$this->_db->close();
		}

		
	}

	public function guardarRespuestas_abcd($id_reg, $id_preg, $respuesta){

		$instruccion = "CALL sp_insertar_respuestas_abcd(".$id_reg.",".$id_preg.",'".$respuesta."')";

		$actualiza=$this->_db->query($instruccion);

		if ($actualiza) {
			return $actualiza; 
			$actualiza->close(); 
			$this->_db->close();
		}

		
	}

	public function guardarRespuestas_multi($id_reg, $id_preg, $respuesta_a, $respuesta_b, $respuesta_c, $respuesta_d){

		$instruccion = "CALL sp_insertar_respuestas_multi(".$id_reg.",".$id_preg.",'".$respuesta_a."','".$respuesta_b."','".$respuesta_c."','".$respuesta_d."')";

		$actualiza=$this->_db->query($instruccion);

		if ($actualiza) {
			return $actualiza; 
			$actualiza->close(); 
			$this->_db->close();
		}

		
	}

	///RESULTADO///
	public function totalencuesta() {

		$instruccion = "CALL sp_total_encuesta";

		$consulta=$this->_db->query($instruccion);
		$resultado=$consulta->fetch_all(MYSQLI_ASSOC);
		if($resultado){
			return $resultado;
			$resultado->close();
			$this->_db->close();
		}else{
			echo "Fallo al consultar las noticias";
		}
	}

	public function totalencuestaSexo() {

		$instruccion = "CALL sp_total_encuesta_sexo";

		$consulta=$this->_db->query($instruccion);
		$resultado=$consulta->fetch_all(MYSQLI_ASSOC);
		if($resultado){
			return $resultado;
			$resultado->close();
			$this->_db->close();
		}else{
			echo "Fallo al consultar las noticias";
		}
	}

	public function consultar_datos() {

		$instruccion = "CALL sp_listar_datos";
		$consulta=$this->_db->query($instruccion);
		$resultado=$consulta->fetch_all(MYSQLI_ASSOC);

		if(!$resultado) {
            echo "Fallo al consultar las noticias";
        }
        else{
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
	}

	public function consultar_datos_filtro($campo, $valor) 
    {
        $instruccion = "CALL sp_listar_datos_filtro('".$campo."','".$valor."')";

        $consulta=$this->_db->query($instruccion);
        $resultado=$consulta->fetch_all(MYSQLI_ASSOC);

        if ($resultado) {
            return $resultado;
            $resultado->close();
            $this->_db->close();
        }
    }


} 