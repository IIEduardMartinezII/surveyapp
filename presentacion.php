<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <title>Ejemplo de formulario maquetado</title>
  <!--<link rel="stylesheet" type="text/css" href="css/main.css">-->
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>


<body>
  <div class="container">

    <h1 class="text-center">Proyecto #1 Desarrollo de Software VII</h1>

    <form name="formularioDatos" method="POST" action="insert.php">
      <div class="form-group">
        <ul>
          <li>
            <label class="titulo" for="nombre">Datos personales: </label>



            <div class="container">
              <div class="row">
                <div class="col-sm">
                  <span class="Rprovincia">
                    <select id="provincia" class="form-control" name="provincia" required="required">
                      <option disabled selected value> -- Elegir provincia -- </option>
                      <option value="Darien">Darien</option>
                      <option value="Colón">Colón</option>
                      <option value="Chiriqui">Chiriqui</option>
                      <option value="Cocle">Cocle</option>
                      <option value="Panamá">Panamá</option>
                      <option value="Panamá Oeste">Panamá Oeste</option>
                      <option value="Veraguas">Veraguas</option>
                      <option value="Bocas del Toro">Bocas del Toro</option>
                      <option value="Herrera">Herrera</option>
                      <option value="Los Santos">Los Santos</option>
                    </select>
                    <label for="provincia"><b>Provincia</b></label>
                  </span>
                </div>

                <div class="col-sm">
                  <span class="Rsalario">
                    <select id="salario" class="form-control" name="salario" required="required">
                      <option disabled selected value> -- Elegir salario -- </option>
                      <option value="0-500">0-500</option>
                      <option value="600-1000">600-1000</option>
                      <option value="1100-1500">1100-1500</option>
                      <option value="1600-2000">1600-2000</option>
                      <option value=">2100">>2100</option>
                    </select>
                    <label for="salario"><b>Salario</b></label>
                  </span>
                </div>

                <div class="col-sm">
                  <span class="Redad">
                    <select id="edad" class="form-control" name="edad" required="required">
                      <option disabled selected value> -- Elegir edad -- </option>
                      <option value="8-15">8-15</option>
                      <option value="16-25">16-25</option>
                      <option value="26-35">26-35</option>
                      <option value="36-45">36-45</option>
                      <option value=">46">>46</option>
                    </select>
                    <label for="edad"><b>Edad</b></label>
                  </span>
                </div>

                <div class="col-sm">
                  <span class="Rsexo">
                    <select id="sexo" class="form-control" name="sexo" required="required">
                      <option disabled selected value> -- Elegir sexo -- </option>
                      <option value="Hombre">Hombre</option>
                      <option value="Mujer">Mujer</option>
                    </select>
                    <label for="sexo"><b>Sexo</b></label>
                  </span>
                </div>
              </div>
            </div>
          </li>
        </ul>

        <?php
        require_once("class/archivo.php");

        //Generar preguntas SINO

        $obj_archivo = new archivo();
        $encuesta = $obj_archivo->listar_preguntas();
        $nfilas = count($encuesta);


        if($nfilas > 0) {

          echo ("<label class='subtitulo' for='nombre'>Escoja la respuesta 'Si' o 'No' para las siguientes preguntas:</label>");
          echo ("<br>");

          echo("<table class='table'>\n");
          echo("<thead class='thead-dark'>\n");
          echo("<tr>\n");

          echo("<th scope='col'>ID</th>\n");
          echo("<th scope='col'>Pregunta</th>\n");
          echo("<th scope='col'>Si</th>\n");
          echo("<th scope='col'>No</th>\n");
          
          
          echo("</tr>\n");
          echo("</thead>\n");
          foreach ($encuesta as $resultado){
            

            echo("<tr>\n");
            echo("<td>".$resultado['id_pregunta']."</td>\n");
            echo("<td>".$resultado['pregunta']."</td>\n");

            echo("<td><input type='radio' name='n".$resultado['id_pregunta']."' value='si' required='required'></td>");
            echo("<td><input type='radio' name='n".$resultado['id_pregunta']."' value='no'></td>");
        
            echo("<tr>\n");
            echo("<tr>\n");
            
            
            echo("</tr>\n");

          }
        
        }

        //Generar preguntas ABCD
        


        $obj_archivo_abcd = new archivo();
        $encuesta_abcd = $obj_archivo_abcd->listar_preguntas_abcd();
        $nfilas_abcd = count($encuesta_abcd);

        if($nfilas_abcd > 0) {

          echo("<table class='table'>\n");
          echo("<thead class='thead-dark'>\n");
          echo("<tr>\n");

          echo("<th scope='col'>ID</th>\n");
          echo("<th scope='col'>Pregunta</th>\n");
          echo("<th scope='col'>A</th>\n");
          echo("<th scope='col'>B</th>\n");
          echo("<th scope='col'>C</th>\n");
          echo("<th scope='col'>D</th>\n");
          
          
          echo("</tr>\n");
          echo("</thead>\n");
          foreach ($encuesta_abcd as $resultado_abcd){

            echo("<tr>\n");
            echo("<td>".$resultado_abcd['id_pregunta']."</td>\n");
            echo("<td>".$resultado_abcd['pregunta']."</td>\n");

            echo("<td>".$resultado_abcd['opc_a']."<br><input type='radio' name='a".$resultado_abcd['id_pregunta']."' value='A' required='required'></td>");
            echo("<td>".$resultado_abcd['opc_b']."<br><input type='radio' name='a".$resultado_abcd['id_pregunta']."' value='B' required='required'></td>");
            echo("<td>".$resultado_abcd['opc_c']."<br><input type='radio' name='a".$resultado_abcd['id_pregunta']."' value='C' required='required'></td>");
            echo("<td>".$resultado_abcd['opc_d']."<br><input type='radio' name='a".$resultado_abcd['id_pregunta']."' value='D' required='required'></td>");

        
            echo("<tr>\n");
            echo("<tr>\n");
            
            
            echo("</tr>\n");

          }

          echo ("<br>");
          echo ("<label class='subtitulo' for='nombre'>Escoja una de las respuestas para las siguientes preguntas:</label>");
          echo ("<br>");
        
        }

        //Generar preguntas multiples

        $obj_archivo_multi = new archivo();
        $encuesta_multi = $obj_archivo_multi->listar_preguntas_multi();
        $nfilas_multi = count($encuesta_multi);

        if($nfilas_multi > 0) {

          echo("<table class='table'>\n");
          echo("<thead class='thead-dark'>\n");
          echo("<tr>\n");

          echo("<th scope='col'>ID</th>\n");
          echo("<th scope='col'>Pregunta</th>\n");
          echo("<th scope='col'>A</th>\n");
          echo("<th scope='col'>B</th>\n");
          echo("<th scope='col'>C</th>\n");
          echo("<th scope='col'>D</th>\n");
          
          
          echo("</tr>\n");
          echo("</thead>\n");

          foreach ($encuesta_multi as $resultado_multi){

            echo("<tr>\n");
            echo("<td>".$resultado_multi['id_pregunta']."</td>\n");
            echo("<td>".$resultado_multi['pregunta']."</td>\n");

            echo("<td>".$resultado_multi['opc_a']."<br><input type='checkbox' name='m1".$resultado_multi['id_pregunta']."' value='TRUE'></td>");
            echo("<td>".$resultado_multi['opc_b']."<br><input type='checkbox' name='m2".$resultado_multi['id_pregunta']."' value='TRUE'></td>");
            echo("<td>".$resultado_multi['opc_c']."<br><input type='checkbox' name='m3".$resultado_multi['id_pregunta']."' value='TRUE'></td>");
            echo("<td>".$resultado_multi['opc_d']."<br><input type='checkbox' name='m4".$resultado_multi['id_pregunta']."' value='TRUE'></td>");

        
            echo("<tr>\n");
            echo("<tr>\n");
            
            
            echo("</tr>\n");

          }

          echo ("<br>");
          echo ("<label class='subtitulo' for='nombre'>Escoja una o más respuestas para las siguientes preguntas:</label>");
          echo ("<br>");

        
        }



        echo("</table>\n");

        echo ("<br>");

        echo ('<div>
        <input value="Enviar" class="btn btn-primary" name="Enviar" type="submit"/>
        <a href="resultados.php" class="btn btn-success">Ver Resultados</a>
        </div>');

        echo("</div>");
        echo("</form>\n");
      
    ?>

      </div>
      <!-- SCRIPT -->
      <script src="js/jquery-migrate-1.4.1.min.js"></script>
      <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <!-- SCRIPT -->
</body>

</html>