<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Resultados</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <h1>
            Results
            <small class="text-muted">Check All Information</small>
        </h1>

        <form name="FormFiltro" method="post" action="resultados.php">
            <div class="form-group">
                <div class="row">
                <div class="col-sm">
                Filtrar por: <select class="form-control" name="campos">
                    <option value="sexo" selected>Sexo
                    <option value="edad">Edad
                    <option value="salario">Rango Salarial
                    <option value="provincia">Provincia
                </select>
                </div>

                <div class="col-sm">
                <div class="form-group">
                Con el valor:
                <input type="text" class="form-control" name="valor">
                <input name="ConsultarFiltro" class="btn btn-outline-secondary btn-sm" style="margin-top: 0.25rem;" value="Filtrar Datos" type="submit" />
                <input name="ConsultarTodos" class="btn btn-outline-secondary btn-sm" style="margin-top: 0.25rem;" value="Ver todos" type="submit" />
                </div>
                </div>
            </div>
        </form>

        <?php
    require_once("class/archivo.php");

    $obj_archivo = new archivo();
    $datos = $obj_archivo->consultar_datos();

    if (array_key_exists('ConsultarTodos', $_POST)) {
        $obj_archivo = new archivo();
        $datos_new = $obj_archivo->consultar_datos();
    }

    if (array_key_exists('ConsultarFiltro', $_POST)) {
        $obj_archivo = new archivo();
        $datos = $obj_archivo->consultar_datos_filtro($_REQUEST['campos'],$_REQUEST['valor']);
    }
    
    $nfilas = count($datos);

    if ($nfilas > 0) {
        print ("<TABLE class='table'>\n");
        print ("<THEAD class='thead-light'>\n");

        print ("<TR>\n");
        //print ("<TH>ID</TH>\n");
        print ("<TH scope='col'>Sexo</TH>\n");
        print ("<TH scope='col'>Edad</TH>\n");
        print ("<TH scope='col'>Salario</TH>\n");
        print ("<TH scope='col'>Provincia</TH>\n");
        print ("</TR>\n");

        print ("</THEAD>\n");

        foreach ($datos as $resultado) {
            print ("<TR>\n");
            //print ("<TD>" .$resultado['id_reg'] . "</TD>\n");
            print ("<TD>" .$resultado['sexo'] . "</TD>\n");
            print ("<TD>" .$resultado['edad'] . "</TD>\n");
            print ("<TD>" .$resultado['salario'] . "</TD>\n");
            print ("<TD>" .$resultado['provincia'] . "</TD>\n");
            print ("</TR>\n");
        }
        print ("</THEAD>\n");
    }else{
        print ("No se han registrado datos");
    }

    //----------- TOTALENCUESTAS -----------//

    $obj_archivo = new archivo();
    $total = $obj_archivo->totalencuesta();
    $totalinfo = count($total);
    if ($totalinfo > 0) {
        echo("<table class='table'>\n");
        print ("<THEAD class='thead-light'>\n");

          echo("<tr>\n");

          echo("<th scope='col'><p class='text-center'>Total Encuestas</p></th>\n");
          echo("</tr>\n");

        print ("</THEAD>\n");

          foreach ($total as $resultado) {
            echo("<tr>\n");
            echo("<td class='text-center'>".$resultado['COUNT(id_reg)']."</td>\n");

            echo("<tr>\n");
            
        
          }
    }

    //----------- TOTALENCUESTAS -----------//
    $obj_archivo = new archivo();
    $sex = $obj_archivo->totalencuestaSexo();
    $totalSex = count($sex);
    if ($totalSex > 0) {
        echo("<table class='table'>\n");
        echo ("<THEAD class='thead-light'>\n");
          echo("<tr>\n");

          echo("<th>Total Encuestas por Mujeres</th>\n");
          echo("<th>Total Encuestas por Hombres</th>\n");
          echo("</tr>\n");

          foreach ($sex as $resultado) {
            echo("<tr>\n");
            echo("<td>".$resultado['Mujer']."</td>\n");
            echo("<td>".$resultado['Hombre']."</td>\n");
            echo("<tr>\n");
            
            print ("</THEAD>\n");
            echo("</tr>\n");
          }
    }

    //---------- ENCUESTAS SEXO ---------//
    echo ('<div class="form-group">
    <a href="presentacion.php" class="btn btn-success">Nueva Encuesta</a>
    </div>');
    
    ?>

        <!-- SCRIPT -->
        <script src="js/jquery-migrate-1.4.1.min.js"></script>
        <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- SCRIPT -->
        
    </div>
</body>

</html>