<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Success</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
    
<?php

require_once("class/archivo.php");

if(array_key_exists('Enviar', $_POST))
{		

    /////////////////////Este rand es para identificar quien hizo que respuesta, funciona como KEY en la BD//////////////////

    $id_reg = rand();

    $Arreglo_ID = [];
    $Arreglo_Resp = [];

    $Arreglo_ID_abcd = [];
    $Arreglo_Resp_abcd  = [];


    $Arreglo_ID_multi = [];
    $Arreglo_Resp_a = [];
    $Arreglo_Resp_b = [];
    $Arreglo_Resp_c = [];
    $Arreglo_Resp_d = [];


    $obj_archivo = new archivo();

    
    $sexo = trim($_POST['sexo']);
    $edad = trim($_POST['edad']);
    $salario = trim($_POST['salario']);
    $provincia = trim($_POST['provincia']);



    /////////////Con esta función guardo solo estos 5 datos en la BD.//////////////////

    $obj_archivo->guardarDatos($id_reg, $sexo, $edad, $salario, $provincia);



    /////////Se llenan 2 arreglos: 1 con el id de la respuesta y el otro con la respuesta	/////////////
    $a = 0;

    for($i=0; $i<21; $i++){


        if (isset($_POST["n".$i.""])){

            $Arreglo_ID[$a] = $i;
            $resp = trim($_POST["n".$i.""]);
            $Arreglo_Resp[$a] = $resp;

            $a = $a + 1;

        }

    }



    ///////////Se guardan los 10 elementos en la BD//////////////////

    for($y=0; $y<4; $y++){

        $id_preg = $Arreglo_ID[$y];
        $respuesta = $Arreglo_Resp[$y];

        $result = $obj_archivo->guardarRespuestas($id_reg, $id_preg, $respuesta);

        if ($result) {


        }

        else{
        print ("<A HREF='presentacion.php'>El SQL formado no funciono, valida MySQL.</A>\n");
        print ($result);
        print("<br/>");
        }

    }





    //////////////Guardado de ID ABCD//////////////////////

    $b = 0;

    for($i=0; $i<21; $i++){

        if (isset($_POST["a".$i.""])){

            $Arreglo_ID_abcd[$b] = $i;
            $resp = trim($_POST["a".$i.""]);
            $Arreglo_Resp_abcd[$b] = $resp;

            $b = $b + 1;

        }

    }

    //Se guardan las 3 respuestas en la base de datos

    for($y=0; $y<3; $y++){

        $id_preg = $Arreglo_ID_abcd[$y];
        $respuesta = $Arreglo_Resp_abcd[$y];

        $result = $obj_archivo->guardarRespuestas_abcd($id_reg, $id_preg, $respuesta);

        if ($result) {


        }

        else{
        print ("<A HREF='presentacion.php'>El SQL formado no funciono, valida MySQL.</A>\n");
        print ($result);
        print("<br/>");
        }

    }

    

    //////////////Guardado de ID MULTI//////////////////////

    $c = 0;

    for($i=0; $i<21; $i++){

        $resp_a = "FALSE";
        $resp_b = "FALSE";
        $resp_c = "FALSE";
        $resp_d = "FALSE";

        if (isset($_POST["m1".$i.""])||isset($_POST["m2".$i.""])||isset($_POST["m3".$i.""])||isset($_POST["m4".$i.""])){

            $Arreglo_ID_multi[$c] = $i;

            if (isset($_POST["m1".$i.""])){

                $resp_a = trim($_POST["m1".$i.""]);

            }

            $Arreglo_Resp_a[$c] = $resp_a;


            if (isset($_POST["m2".$i.""])){

                $resp_b = trim($_POST["m2".$i.""]);
            
            }

            $Arreglo_Resp_b[$c] = $resp_b;

            if (isset($_POST["m3".$i.""])){

                $resp_c = trim($_POST["m3".$i.""]);

            }

            $Arreglo_Resp_c[$c] = $resp_c;


            if (isset($_POST["m4".$i.""])){

                $resp_d = trim($_POST["m4".$i.""]);

            }

            $Arreglo_Resp_d[$c] = $resp_d;


            if($resp_a == "TRUE"||$resp_b == "TRUE"||$resp_c == "TRUE"||$resp_d == "TRUE"){

                $c = $c + 1;

            }
            else{
                print ("<A HREF='presentacion.php'>Debes llenar todas las casillas de seleccion multiple!.</A>\n");
            }

        }

    }

    //Se guardan las respuestas en la base de datos

    for($y=0; $y<3; $y++){

        $id_preg = $Arreglo_ID_multi[$y];

        $respuesta_a = $Arreglo_Resp_a[$y];
        $respuesta_b = $Arreglo_Resp_b[$y];
        $respuesta_c = $Arreglo_Resp_c[$y];
        $respuesta_d = $Arreglo_Resp_c[$y];


        $result = $obj_archivo->guardarRespuestas_multi($id_reg, $id_preg, $respuesta_a, $respuesta_b, $respuesta_c, $respuesta_d);

        if ($result) {


        }

        else{
        print ("<A HREF='presentacion.php'>El SQL formado no funciono, valida MySQL.</A>\n");
        print ($result);
        print("<br/>");
        }

    }

    

	print("<h1 class='text-center'>Se guardaron los elementos en la base de datos.</h1>");
    print("<br/>");	
	echo ('<div class="form-group">
	<a href="presentacion.php" class="btn btn-primary">Nueva Encuesta</a>
	<a href="resultados.php" class="btn btn-success">Ver Resultados</a>
    </div>');


}


else{

	print("<h1 class='text-center'>El POST no traia contenido, ¿iniciaste en presentación.php?</h1>");

}


?>


    <!-- SCRIPT -->
    <script src="js/jquery-migrate-1.4.1.min.js"></script>
    <script src="https://unpkg.com/@popperjs/core@2/dist/umd/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- SCRIPT -->
</body>
</html>
